-- Table: COURSES
CREATE TABLE IF NOT EXISTS courses(
	id_course INT AUTO_INCREMENT PRIMARY KEY,
	course_name VARCHAR(40));

-- Table: PERSONS
CREATE TABLE IF NOT EXISTS persons(
	id_person INT AUTO_INCREMENT PRIMARY KEY,
	first_name VARCHAR(40),
	last_name VARCHAR(40),
	email VARCHAR(255),
	password VARCHAR(15),
	course_id INT,
	role ENUM('Audzēknis', 'Skolotājs', 'Pārvaldnieks'),
	FOREIGN KEY (course_id) REFERENCES courses(id_course) ON UPDATE CASCADE ON DELETE CASCADE);

-- Table: SUBJECTS
CREATE TABLE IF NOT EXISTS subjects(
	id_subject INT AUTO_INCREMENT PRIMARY KEY,
	subject_name VARCHAR(255));

-- Table: TEACHER_SUBJECTS
CREATE TABLE IF NOT EXISTS teacher_subjects(
	id_ts INT AUTO_INCREMENT PRIMARY KEY,
	teacher_id INT,
	subject_id INT,
	FOREIGN KEY (teacher_id) REFERENCES persons(id_person) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (subject_id) REFERENCES subjects(id_subject) ON UPDATE CASCADE ON DELETE CASCADE);

-- Table: CONSULTATIONS
CREATE TABLE IF NOT EXISTS consultations(
	id_consultation INT AUTO_INCREMENT PRIMARY KEY,
	student_id INT,
	subject_id INT,
	teacher_id INT,
	topic VARCHAR(255),
	date_on date,
	rewrite TINYINT(1),
	old_mark VARCHAR(5),
	rewrite_date date,
	FOREIGN KEY (student_id) REFERENCES persons(id_person) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (subject_id) REFERENCES subjects(id_subject) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (teacher_id) REFERENCES persons(id_person) ON UPDATE CASCADE ON DELETE CASCADE);
    
-- INSERT INTO courses
INSERT INTO courses (course_name) VALUES ('2.p');

-- INSERT INTO persons
INSERT INTO persons (first_name, last_name, email, password, role) VALUES ('Konsultācijas', 'Admin', 'eksamens2022@jak.lv', 'eksamens2022', 'Pārvaldnieks');
INSERT INTO persons (first_name, last_name, email, password, role) VALUES ('Aigars', 'Ašaks', 'aigars.asaks@jak.lv', 'aigars2022', 'Skolotājs');
INSERT INTO persons (first_name, last_name, email, password, course_id, role) VALUES ('Artis', 'Bērzs', 'berzs.artis@jak.lv', 'berzs2022', 1, 'Audzēknis');

-- INSERT INTO subjects
INSERT INTO subjects (subject_name) VALUES ('Sistēmu programmēšana');
INSERT INTO subjects (subject_name) VALUES ('Matemātika');

-- INSERT INTO teacher_subjects
INSERT INTO teacher_subjects (teacher_id, subject_id) VALUES (2, 1);
INSERT INTO teacher_subjects (teacher_id, subject_id) VALUES (2, 2);

-- INSERT INTO consultations
INSERT INTO consultations (student_id, subject_id, teacher_id, topic, date_on) VALUES (3, 1, 2, 'Paralēlās plūsmas', date('2022-06-17'));
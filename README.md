# Programmatūras apraksts
Programmatūra paredzēta CRUD metadoloģijas demonstrācijai WEB vidē. Tā tika izveidota mācību ietvaros kā eksāmena darbs lai demonstrētu prasmes, veidojot servisus, kuri izpilda CRUD galvenos principus:

- ievietot datus
- atjaunot eksistējošus datus
- dzēst eksistējošus datus
- iegūt eksistējošu datu unikālos identifikātorus
- iegūt eksistējošos datus pēc unikālā identifikatora
- iegūt visus eksistējošos datus
- iegūt eksistējošos datus pēc noteiktiem parametriem kā: nosaukums, audzinatajs, klase, talr. nr., ē-pasts u.c.

Visi dati no datubāzes tiek tabulas formas veidā atrādīti JAVA aplikācijā. Katram datubāzes laukam ir savs attiecīgais form.java skripts, kas nodrošina lietotāja datu apstrādi.

Lai šo programmatūru pielietotu ir nepieciešama SQL datubāze, kuru var izveidot palaižot database.sql skripta kodu.
Programmas izstrādes ietvaros tika pielietotas šādas tehnoloģijas:
- JAVA 19
- MySql

JAVA tika izvirzīta kā nepieciešamā programmēšanas valoda priekš programmatūras izstrādes.
Datubāzes datu apstrādei tika pilietots MySQL nevis PostgreSQL, jo projekta ietvaros tā lietošana nebija nepieciešama, bet programmatūru būtu labāk izveidot uz PostgreSQL lai ātrāk apstrādāt lielas datu kopas, sarežģītus vaicājumus un lasīšanas-rakstīšanas darbības.
Tā kā tas nebija liels projekts, bet tieši projekts mācību ietvaros, tā vizuālajam noformējumam nebija likts tik liels uzsvars.

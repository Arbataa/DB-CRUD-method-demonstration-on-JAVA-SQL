package lv.jak.eksamens.forms;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import lv.jak.eksamens.data.Course;
import lv.jak.eksamens.data.Person;
import lv.jak.eksamens.data.Subject;
import lv.jak.eksamens.data.TeacherSubject;
import lv.jak.eksamens.services.CourseService;
import lv.jak.eksamens.services.PersonService;
import lv.jak.eksamens.services.SubjectService;
import lv.jak.eksamens.services.TeacherSubjectService;

import javax.swing.JSeparator;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;

//Nav komentāru. Komentāriem jābūt paskaidrošiem un konkrētiem, lai attēlotu metodes, funkciju darbības principus.
//Nav ievērots nozīmīgu mainīgo nosaukumu princips. Mainīgo nosaukumiem ir jābūt jēgpilniem un saprotamiem kodā.
//Nav ievērots funkciju, metožu pārpildīšanas princips. Koda primcipam jābūt sadalītam vairākās funkcijās vai metodēs pēc to nozīmēm.
//Nav ievērots CamelCase princips. CamelCase princips nodrošina to, ka mainīgo, metožu un funkciju nosaukumi sākas ar mazo burtu un ,sekojot katram nākamajam vārdam, to vārdu nosaukumi ir jāraksta ar lielo burtu.

public class AdminForm extends JFrame {
	private static final long serialVersionUID = 1640880952980309925L;

	private Connection conn;

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;

	DefaultListModel<String> teacherModel = new DefaultListModel<String>();
	DefaultListModel<String> teacherSubjectsModel = new DefaultListModel<String>();

	DefaultListModel<String> kurssModel = new DefaultListModel<String>();
	DefaultListModel<String> macPrieksModel = new DefaultListModel<String>();
	DefaultListModel<String> audzModel = new DefaultListModel<String>();

	private DefaultComboBoxModel<String> comboBoxModel = new DefaultComboBoxModel<String>();

	public AdminForm(Connection conn, String user) throws SQLException {
		this.conn = conn;

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Konsultāciju reģistrēšanas sistēma");
		setBounds(100, 100, 1200, 800);
		setLocationRelativeTo(null);
		setResizable(false);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		JSeparator separator = new JSeparator();
		separator.setBounds(0, 58, 1184, 2);
		contentPane.add(separator);

		JLabel lblUser = new JLabel("User: " + user);
		lblUser.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblUser.setBounds(12, 12, 652, 34);
		contentPane.add(lblUser);

		JButton btnNewButton = new JButton("Iziet");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginForm frame = new LoginForm(conn);
				frame.setVisible(true);
				setVisible(false);
			}
		});
		btnNewButton.setBounds(1115, 12, 57, 33);
		contentPane.add(btnNewButton);

		JLabel lblKursuDati = new JLabel("Kursu dati");
		lblKursuDati.setFont(new Font("Dialog", Font.BOLD, 20));
		lblKursuDati.setBounds(10, 73, 115, 34);
		contentPane.add(lblKursuDati);

		JButton btnPievienot = new JButton("Pievienot");
		btnPievienot.setBounds(12, 120, 98, 26);
		contentPane.add(btnPievienot);

		JButton btnLabot = new JButton("Labot");
		btnLabot.setBounds(12, 159, 98, 26);
		contentPane.add(btnLabot);

		JButton btnDzst = new JButton("Dzēst");
		btnDzst.setBounds(12, 198, 98, 26);
		contentPane.add(btnDzst);

		JLabel lblNosaukums = new JLabel("Nosaukums");
		lblNosaukums.setHorizontalAlignment(SwingConstants.TRAILING);
		lblNosaukums.setBounds(12, 237, 75, 16);
		contentPane.add(lblNosaukums);

		textField = new JTextField();
		textField.setBounds(93, 235, 57, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		JLabel lblKursuSaraksts = new JLabel("Kursu saraksts");
		lblKursuSaraksts.setHorizontalAlignment(SwingConstants.CENTER);
		lblKursuSaraksts.setBounds(191, 73, 98, 16);
		contentPane.add(lblKursuSaraksts);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(191, 102, 98, 190);
		contentPane.add(scrollPane);

		JList<String> list = new JList<String>(kurssModel);
		scrollPane.setViewportView(list);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(12, 491, 316, 257);
		contentPane.add(scrollPane_1);

		JList<String> list_1 = new JList<String>(macPrieksModel);
		scrollPane_1.setViewportView(list_1);

		JLabel lblMcbuPriekmetuSaraksts = new JLabel("Mācību priekšmetu saraksts");
		lblMcbuPriekmetuSaraksts.setHorizontalAlignment(SwingConstants.CENTER);
		lblMcbuPriekmetuSaraksts.setBounds(12, 462, 316, 16);
		contentPane.add(lblMcbuPriekmetuSaraksts);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(109, 417, 219, 20);
		contentPane.add(textField_1);

		JLabel lblNosaukums_1 = new JLabel("Nosaukums");
		lblNosaukums_1.setHorizontalAlignment(SwingConstants.TRAILING);
		lblNosaukums_1.setBounds(12, 419, 85, 16);
		contentPane.add(lblNosaukums_1);

		JLabel lblMcbuPriekmetuDati = new JLabel("Mācību priekšmetu dati");
		lblMcbuPriekmetuDati.setFont(new Font("Dialog", Font.BOLD, 20));
		lblMcbuPriekmetuDati.setBounds(12, 333, 387, 34);
		contentPane.add(lblMcbuPriekmetuDati);

		JButton btnPievienot_1 = new JButton("Pievienot");
		btnPievienot_1.setBounds(12, 380, 98, 26);
		contentPane.add(btnPievienot_1);

		JButton btnLabot_1 = new JButton("Labot");
		btnLabot_1.setBounds(124, 380, 98, 26);
		contentPane.add(btnLabot_1);

		JButton btnDzst_1 = new JButton("Dzēst");
		btnDzst_1.setBounds(230, 380, 98, 26);
		contentPane.add(btnDzst_1);

		JLabel lblPersonasDatiDati = new JLabel("Personas dati");
		lblPersonasDatiDati.setFont(new Font("Dialog", Font.BOLD, 20));
		lblPersonasDatiDati.setBounds(396, 73, 140, 34);
		contentPane.add(lblPersonasDatiDati);

		JLabel lblNosaukums_1_1 = new JLabel("Vārds");
		lblNosaukums_1_1.setHorizontalAlignment(SwingConstants.TRAILING);
		lblNosaukums_1_1.setBounds(396, 161, 85, 16);
		contentPane.add(lblNosaukums_1_1);

		JLabel lblNosaukums_1_2 = new JLabel("Uzvārds");
		lblNosaukums_1_2.setHorizontalAlignment(SwingConstants.TRAILING);
		lblNosaukums_1_2.setBounds(396, 190, 85, 16);
		contentPane.add(lblNosaukums_1_2);

		JLabel lblNosaukums_1_3 = new JLabel("E-pasts");
		lblNosaukums_1_3.setHorizontalAlignment(SwingConstants.TRAILING);
		lblNosaukums_1_3.setBounds(396, 218, 85, 16);
		contentPane.add(lblNosaukums_1_3);

		JLabel lblNosaukums_1_4 = new JLabel("Parole");
		lblNosaukums_1_4.setHorizontalAlignment(SwingConstants.TRAILING);
		lblNosaukums_1_4.setBounds(396, 247, 85, 16);
		contentPane.add(lblNosaukums_1_4);

		JLabel lblNosaukums_1_5 = new JLabel("Kurss");
		lblNosaukums_1_5.setHorizontalAlignment(SwingConstants.TRAILING);
		lblNosaukums_1_5.setBounds(396, 276, 85, 16);
		contentPane.add(lblNosaukums_1_5);

		textField_2 = new JTextField();
		textField_2.setBounds(493, 159, 219, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(493, 188, 219, 20);
		contentPane.add(textField_3);

		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(493, 217, 219, 20);
		contentPane.add(textField_4);

		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(493, 245, 219, 20);
		contentPane.add(textField_5);

		JComboBox<String> comboBox = new JComboBox<String>(comboBoxModel);
		comboBox.setBounds(493, 272, 75, 25);
		comboBox.setEnabled(false);
		contentPane.add(comboBox);

		JRadioButton rdbtnAudzeknis = new JRadioButton("Audzēknis");
		rdbtnAudzeknis.setBounds(532, 308, 85, 24);
		rdbtnAudzeknis.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				comboBox.setEnabled(true);
			}
		});
		contentPane.add(rdbtnAudzeknis);

		JRadioButton rdbtnSkolotjs = new JRadioButton("Skolotājs");
		rdbtnSkolotjs.setBounds(634, 308, 78, 24);
		rdbtnSkolotjs.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				comboBox.setEnabled(false);
			}
		});
		contentPane.add(rdbtnSkolotjs);

		ButtonGroup bg = new ButtonGroup();
		bg.add(rdbtnAudzeknis);
		bg.add(rdbtnSkolotjs);

		JButton btnPievienot_1_1 = new JButton("Pievienot");
		btnPievienot_1_1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				PersonService persServ = new PersonService(conn);
				String vards = textField_2.getText();
				String uzvards = textField_3.getText();
				String epasts = textField_4.getText();
				String parole = textField_5.getText();
				String kurss = comboBox.getSelectedItem().toString();

				if (rdbtnAudzeknis.isSelected()) {
					try {
						persServ.insertPerson(vards, uzvards, epasts, parole, kurss);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} else if (rdbtnSkolotjs.isSelected()) {
					try {
						persServ.insertPerson(vards, uzvards, epasts, parole);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		btnPievienot_1_1.setBounds(396, 120, 98, 26);
		contentPane.add(btnPievienot_1_1);

		JButton btnLabot_1_1 = new JButton("Labot");
		btnLabot_1_1.setBounds(508, 120, 98, 26);
		contentPane.add(btnLabot_1_1);

		JLabel lblAudzkuSaraksts = new JLabel("Audzēkņu saraksts");
		lblAudzkuSaraksts.setHorizontalAlignment(SwingConstants.CENTER);
		lblAudzkuSaraksts.setBounds(396, 345, 140, 16);
		contentPane.add(lblAudzkuSaraksts);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(396, 374, 140, 190);
		contentPane.add(scrollPane_2);

		JList<String> list_2 = new JList<String>(audzModel);
		scrollPane_2.setViewportView(list_2);

		JLabel lblSkolotjuSaraksts = new JLabel("Skolotāju saraksts");
		lblSkolotjuSaraksts.setHorizontalAlignment(SwingConstants.CENTER);
		lblSkolotjuSaraksts.setBounds(572, 345, 140, 16);
		contentPane.add(lblSkolotjuSaraksts);
		
		JScrollPane scrollPane_2_1 = new JScrollPane();
		scrollPane_2_1.setBounds(572, 374, 140, 190);
		contentPane.add(scrollPane_2_1);

		JList<String> list_3 = new JList<String>(teacherModel);
		scrollPane_2_1.setViewportView(list_3);
		
		JButton btnDzst_1_1 = new JButton("Dzēst");
		btnDzst_1_1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				PersonService persServ = new PersonService(conn);
				if (!(list_2.getSelectedValue() == null)) {
					String[] persona = list_2.getSelectedValue().split(" ");
					String vards = persona[0];
					String uzvards = persona[1];
					try {
						persServ.deletePerson(vards, uzvards);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				if (!(list_3.getSelectedValue() == null)) {
					String[] persona = list_2.getSelectedValue().split(" ");
					String vards = persona[0];
					String uzvards = persona[1];
					try {
						persServ.deletePerson(vards, uzvards);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}	
			}
		});
		btnDzst_1_1.setBounds(614, 120, 98, 26);
		contentPane.add(btnDzst_1_1);

		PersonService personService = new PersonService(conn);
		List<Person> teacherList = personService.getPersonsByRole("Skolotājs");
		for (Person person : teacherList) {
			String firstName = person.getFirstName();
			String lastName = person.getLastName();
			String teacher = firstName + " " + lastName;
			teacherModel.addElement(teacher);
		}

		JLabel lblMcbuPriekmetuPiesaiste = new JLabel("Mācību priekšmetu piesaiste");
		lblMcbuPriekmetuPiesaiste.setFont(new Font("Dialog", Font.BOLD, 20));
		lblMcbuPriekmetuPiesaiste.setBounds(396, 587, 316, 34);
		contentPane.add(lblMcbuPriekmetuPiesaiste);

		JLabel lblIzvlietiesSkolotju = new JLabel("Izvēlieties skolotāju");
		lblIzvlietiesSkolotju.setBounds(396, 633, 124, 16);
		contentPane.add(lblIzvlietiesSkolotju);

		JComboBox<String> comboBox_1 = new JComboBox<String>();
		comboBox_1.setBounds(525, 629, 187, 25);
		contentPane.add(comboBox_1);

		JLabel lblNoMcbuPriekmeta = new JLabel("No mācību priekšmeta saraksta izvēlieties mācību");
		lblNoMcbuPriekmeta.setBounds(396, 670, 316, 16);
		contentPane.add(lblNoMcbuPriekmeta);

		JLabel lblNewLabel = new JLabel("priekšmetus, kurus mācīs izvēlētais skolotājs.");
		lblNewLabel.setBounds(396, 689, 316, 16);
		contentPane.add(lblNewLabel);

		JButton btnApstiprint = new JButton("Apstiprināt");
		btnApstiprint.setBounds(396, 722, 98, 26);
		contentPane.add(btnApstiprint);

		JSeparator separator_1 = new JSeparator();
		separator_1.setOrientation(SwingConstants.VERTICAL);
		separator_1.setBounds(734, 58, 2, 703);
		contentPane.add(separator_1);

		JLabel lblSkolotjaMcbuPriekmetu = new JLabel("Skolotāju mācību priekšmetu saraksts");
		lblSkolotjaMcbuPriekmetu.setFont(new Font("Dialog", Font.BOLD, 20));
		lblSkolotjaMcbuPriekmetu.setBounds(754, 73, 418, 34);
		contentPane.add(lblSkolotjaMcbuPriekmetu);

		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(748, 120, 424, 629);
		contentPane.add(scrollPane_3);

		TeacherSubjectService teacherSubjectService = new TeacherSubjectService(conn);

		List<TeacherSubject> teacherSubjectsList = teacherSubjectService.getAllTeacherSubjects();
		for (TeacherSubject ts : teacherSubjectsList) {
			String firstName = ts.getTeacher().getFirstName();
			String lastName = ts.getTeacher().getLastName();
			String subject = ts.getSubject().getName();

			String value = firstName + " " + lastName + " - " + subject;
			teacherSubjectsModel.addElement(value);
		}

		JList<String> list_4 = new JList<String>(teacherSubjectsModel);
		scrollPane_3.setViewportView(list_4);

		fillKurssModel();
		fillMacPrieksModel();
		fillAudzModel();
		fillComboBoxModel();
	}

	public void fillKurssModel() throws SQLException {
		CourseService kursServ = new CourseService(conn);
		List<Course> kursi = new ArrayList<Course>();
		kursi.addAll(kursServ.getAllCourses());
		for (Course course : kursi) {
			kurssModel.addElement(course.getName());
		}

	}

	public void fillMacPrieksModel() throws SQLException {
		SubjectService macPrieksServ = new SubjectService(conn);
		List<Subject> prieksmeti = new ArrayList<Subject>();
		prieksmeti.addAll(macPrieksServ.getAllSubjects());
		for (Subject subject : prieksmeti) {
			macPrieksModel.addElement(subject.getName());
		}

	}

	public void fillAudzModel() throws SQLException {
		PersonService persServ = new PersonService(conn);

		List<Person> audzekni = new ArrayList<Person>();
		audzekni.addAll(persServ.getAllPersons());
		for (Person person : audzekni) {
			if (person.getRole().toString().equals("Audzēknis")) {
				audzModel.addElement(person.getFirstName() + " " + person.getLastName() + " - " + person.getCourse().getName());
			}
		}

	}

	private void fillComboBoxModel() throws SQLException {
		CourseService coursServ = new CourseService(conn);
		for (Course course : coursServ.getAllCourses()) {
			comboBoxModel.addElement(course.getName());
		}
	}

}

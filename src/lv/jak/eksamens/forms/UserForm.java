package lv.jak.eksamens.forms;

import java.sql.Connection;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class UserForm extends JFrame {
	private static final long serialVersionUID = 6580379591331448859L;
	
	private Connection conn;
	
	private JPanel contentPane;

	public UserForm(Connection conn) {
		this.conn = conn;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setTitle("Konsultāciju reģistrēšanas sistēma");
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
	}
}
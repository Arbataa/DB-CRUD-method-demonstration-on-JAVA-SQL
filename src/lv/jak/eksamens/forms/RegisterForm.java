package lv.jak.eksamens.forms;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import lv.jak.eksamens.data.Course;
import lv.jak.eksamens.services.CourseService;
import lv.jak.eksamens.services.PersonService;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

public class RegisterForm extends JFrame {
	private static final long serialVersionUID = 1815699647841370106L;
	
	private Connection conn;
	
	private DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
	private ButtonGroup btnGroup = new ButtonGroup();
	
	String vards,uzvards,epasts,parole,kurss,lietotajs;
	
	
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	
	
	public RegisterForm(Connection conn) throws SQLException {
		this.conn = conn;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Konsultāciju reģistrēšanas sistēma");
		setBounds(100, 100, 524, 201);
		setLocationRelativeTo(null);
		setResizable(false);
		
		fillModel();
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		textField = new JTextField();
		textField.setBounds(83, 11, 184, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Vārds");
		lblNewLabel.setBounds(18, 11, 55, 16);
		contentPane.add(lblNewLabel);
		
		JLabel lblUzvrds = new JLabel("Uzvārds");
		lblUzvrds.setBounds(18, 38, 55, 16);
		contentPane.add(lblUzvrds);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(83, 38, 184, 20);
		contentPane.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(83, 65, 184, 20);
		contentPane.add(textField_2);
		
		JLabel lblEpasts = new JLabel("e-pasts");
		lblEpasts.setBounds(18, 65, 55, 16);
		contentPane.add(lblEpasts);
		
		JLabel lblParole = new JLabel("Parole");
		lblParole.setBounds(18, 96, 55, 16);
		contentPane.add(lblParole);
		
		JLabel lblAtkrtojietParoli = new JLabel("Atkārtojiet paroli");
		lblAtkrtojietParoli.setBounds(18, 126, 103, 16);
		contentPane.add(lblAtkrtojietParoli);
		
		JLabel lblNewLabel_1 = new JLabel("Kurss");
		lblNewLabel_1.setBounds(293, 13, 55, 16);
		contentPane.add(lblNewLabel_1);
		
		JComboBox comboBox = new JComboBox(model);
		comboBox.setBounds(366, 9, 113, 25);
		contentPane.add(comboBox);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Skolotājs");
		rdbtnNewRadioButton.setBounds(401, 42, 78, 24);
		rdbtnNewRadioButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				comboBox.setEnabled(false);
				
			}
		});
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnAudzknis = new JRadioButton("Audzēknis");
		rdbtnAudzknis.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				comboBox.setEnabled(true);
				
			}
		});
		rdbtnAudzknis.setBounds(303, 42, 84, 24);
		contentPane.add(rdbtnAudzknis);
		
		btnGroup.add(rdbtnAudzknis);
		btnGroup.add(rdbtnNewRadioButton);
		
		JButton btnNewButton = new JButton("Reģistrēties");
		btnNewButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				vards = textField.getText();
				uzvards = textField_1.getText();
				epasts = textField_2.getText();
				
				char[] passwordValueFirst = passwordField.getPassword();
				String passwordFirst = "";
				for (char c : passwordValueFirst) {
					passwordFirst += c;
				}
				char[] passwordValueSecond = passwordField_1.getPassword();
				String passwordSecond = "";
				for (char c : passwordValueSecond) {
					passwordSecond += c;
				}
				
				if(rdbtnNewRadioButton.isSelected()) {

					if(passwordFirst.equals(passwordSecond)) {
						PersonService persServ = new PersonService(conn);
						try {
							persServ.insertPerson(vards, uzvards, epasts, passwordSecond);
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					
					}
					
				}else {
					if(passwordFirst.equals(passwordSecond)) {
						
						kurss = comboBox.getSelectedItem().toString();
						PersonService persServ = new PersonService(conn);
						
						try {
							persServ.insertPerson(vards, uzvards, epasts, passwordSecond, kurss);
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
				LoginForm frame = null;
				frame = new LoginForm(conn);
				frame.setVisible(true);
				setVisible(false);
			}
		});
		btnNewButton.setBounds(366, 91, 113, 26);
		contentPane.add(btnNewButton);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(83, 94, 184, 18);
		contentPane.add(passwordField);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(133, 124, 184, 18);
		contentPane.add(passwordField_1);
	}
	
	private void fillModel() throws SQLException {
		CourseService coursServ = new CourseService(conn);
		for (Course course : coursServ.getAllCourses()) {
			model.addElement(course.getName());
		}
	}

}
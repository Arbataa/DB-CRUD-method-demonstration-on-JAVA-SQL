package lv.jak.eksamens.forms;

import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import lv.jak.eksamens.data.Person;
import lv.jak.eksamens.services.PersonService;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;

public class LoginForm extends JFrame {
	private static final long serialVersionUID = -6741171137017704470L;
	
	private Connection conn;
	
	private JPanel contentPane;
	private JTextField emailTF;
	private JPasswordField passwordPF;
	
	public LoginForm(Connection conn) {
		this.conn = conn;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Konsultāciju reģistrēšanas sistēma");
		setBounds(100, 100, 480, 290);
		setLocationRelativeTo(null);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		
		init();
	}
	
	private void init() {
		JLabel headerLBL = new JLabel("Konsultāciju reģistrācijas sistēma");
		headerLBL.setHorizontalAlignment(SwingConstants.CENTER);
		headerLBL.setFont(new Font("Tahoma", Font.ITALIC, 26));
		headerLBL.setBounds(10, 30, 444, 44);
		contentPane.add(headerLBL);
		
		JLabel emailLBL = new JLabel("E-pasts:");
		emailLBL.setHorizontalAlignment(SwingConstants.RIGHT);
		emailLBL.setFont(new Font("Tahoma", Font.PLAIN, 12));
		emailLBL.setBounds(125, 111, 45, 14);
		contentPane.add(emailLBL);
		
		emailTF = new JTextField();
		emailTF.setBounds(180, 108, 150, 20);
		contentPane.add(emailTF);
		emailTF.setColumns(10);
		
		JLabel passwordLBL = new JLabel("Parole:");
		passwordLBL.setHorizontalAlignment(SwingConstants.RIGHT);
		passwordLBL.setFont(new Font("Tahoma", Font.PLAIN, 12));
		passwordLBL.setBounds(125, 136, 45, 14);
		contentPane.add(passwordLBL);
		
		passwordPF = new JPasswordField();
		passwordPF.setColumns(10);
		passwordPF.setBounds(180, 133, 150, 20);
		contentPane.add(passwordPF);
		
		JButton loginBTN = new JButton("Autorizēties");
		loginBTN.setBounds(220, 186, 110, 26);
		loginBTN.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String eMail = emailTF.getText();
				char[] passwordValue = passwordPF.getPassword();
				String password = "";
				for (char c : passwordValue) {
					password += c;
				}
				
				PersonService personService = new PersonService(conn);
				Person person = null;
				try {
					person = personService.getPersonByLogin(eMail, password);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				if (person != null) {
					try {
						Object frame;
						if(person.getRole().equals("Pārvaldnieks")) {
							String user = person.getFirstName() + " " + person.getLastName();
							frame = new AdminForm(conn, user);
						}else {
							frame = new UserForm(conn);
						}
						((Window) frame).setVisible(true);
						setVisible(false);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		contentPane.add(loginBTN);
		
		JButton registerBTN = new JButton("Reģistrēties");
		registerBTN.setBounds(95, 186, 110, 26);
		registerBTN.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				RegisterForm frame = null;
				try {
					frame = new RegisterForm(conn);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				frame.setVisible(true);
				setVisible(false);
			}
		});
		contentPane.add(registerBTN);
	}
}
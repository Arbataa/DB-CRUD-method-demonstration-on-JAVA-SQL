package lv.jak.eksamens.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import lv.jak.eksamens.data.Course;

public class CourseService {
	public Connection conn;
	public CourseService(Connection conn) {
		this.conn = conn;
	}
	
	// 1. uzdevums
	public boolean insertCourse(String name) throws SQLException {
String sql = "INSERT INTO courses(course_name) VALUES (?)";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setString(1, name);
		
		boolean result = stmt.execute();
		return result;
	}
	
	public int updateCourse(String oldName, String newName) throws SQLException {
		String sql = "UPDATE courses SET course_name=? WHERE course_name=?";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setString(1, newName);
		stmt.setString(2, oldName);
		
		int result = stmt.executeUpdate();
		return result;
	}
	
	public int deleteCourse(String name) throws SQLException {
		String sql = "DELETE FROM courses WHERE course_name=?";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setString(1, name);
		
		int result = stmt.executeUpdate();
		return result;
	}
	
	public int getCourseID(String name) throws SQLException {
        String sql = "SELECT id_course FROM courses WHERE course_name=?";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setString(1, name);
		
		ResultSet rs = stmt.executeQuery();
		rs.first();
		
		int id = rs.getInt("id_course");
		return id;
	}
	
	public Course getCourseByID(int id) throws SQLException {
		String sql = "SELECT * FROM courses WHERE id_course=?";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setInt(1,id);
		
		ResultSet rs = stmt.executeQuery();
		rs.first();
		
		String name = rs.getString("course_name");		
		Course course = new Course(name);
		course.setID(id);
				
		return course;
	}
	
	public List<Course> getAllCourses() throws SQLException {
		List<Course> courses = new ArrayList<Course>();
		
		String sql = "SELECT * FROM courses";
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		ResultSet rs = stmt.executeQuery();
		
		while(rs.next()) {
			int id = rs.getInt("id_course");
			String name = rs.getString("course_name");
			
			Course course = new Course(name);
			course.setID(id);
			
			courses.add(course);
		}
		return courses;
	}
}
package lv.jak.eksamens.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import lv.jak.eksamens.data.Course;
import lv.jak.eksamens.data.Person;

public class PersonService {
	private Connection conn;
	
	public PersonService(Connection conn) {
		this.conn = conn;
	}
	
	
	/*
	 * Metode nodrošina jauna audzēkņa ieraksta ievietošanu konsultacijas datubāzē, persons tabulā.
	 * Metode atgriež boolean vērtību, kas funkcija ir definēt, ka inserPerson metode izpildījās.
	 * firstName - DB persons tabulas first_name lauka aizpildīšanas parametrs
	 * lastName - DB persons tabulas last_name lauka aizpildīšanas parametrs
	 * email - DB persons tabulas email lauka aizpildīšanas parametrs
	 * password - DB persons tabulas lauka password aizpildīšanas parametrs
	 * course - DB persons tabulas lauka course_id aizpildīšanas parametrs
	 */
	public boolean insertPerson(String firstName, String lastName, String eMail, String password, String course) throws SQLException {
		CourseService courseService = new CourseService(conn);
		int courseID = courseService.getCourseID(course);
		
		String sql = "INSERT INTO persons(first_name,last_name,email,password,course_id, role) VALUES (?, ?, ?, ?, ?, 'Audzēknis')";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setString(1, firstName);
		stmt.setString(2, lastName);
		stmt.setString(3, eMail);
		stmt.setString(4, password);
		stmt.setInt(5, courseID);
		
		boolean result = stmt.execute();
		
		return result;
	}
	
	public int deletePerson(String firstName, String lastName) throws SQLException {
		String sql = "DELETE FROM persons WHERE first_name=? AND last_name=?";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setString(1, firstName);
		stmt.setString(2, lastName);
		
		int result = stmt.executeUpdate();
		return result;
	}
	
	public boolean insertPerson(String firstName, String lastName, String eMail, String password) throws SQLException {
		String sql = "INSERT INTO persons(first_name, last_name, email, password, role) VALUES (?, ?, ?, ?, 'Skolotājs')";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setString(1, firstName);
		stmt.setString(2, lastName);
		stmt.setString(3, eMail);
		stmt.setString(4, password);
		
		boolean result = stmt.execute();
		
		return result;
	}
	/*
	 * Metode nodrošina SQL vaicājuma izveidošanu, kurā tiek uzradīta persona pēc tā ID.
	 * Metode atgriež person klases objektu, kas metodes izpildes procesā tiek aizpildīts ar vērtībām, kuras iegūst no vaicājuma.
	 * id - parametrs, kuru pielieto SQL vaicājumā, lai atrastu personu pēc id.
	 */
	public Person getPersonByID(int id) throws SQLException {
		String sql = "SELECT * FROM persons WHERE id_person=?";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setInt(1, id);
		
		ResultSet rs = stmt.executeQuery();
		rs.first();
		
		String firstName = rs.getString("first_name");
		String lastName = rs.getString("last_name");
		String eMail = rs.getString("email");
		String password = rs.getString("password");
		int courseID = rs.getInt("course_id");
		String role = rs.getString("role");
		
		Person person = new Person(firstName, lastName, eMail, password);
		person.setID(id);
		person.setRole(role);
		if(courseID != 0) {
			CourseService courseService = new CourseService(conn);
			Course course = courseService.getCourseByID(courseID);
			
			person.setCourse(course);
		}
		
		return person;
	}
	
	public int getPersonID(String firstName, String lastName) throws SQLException {
		String sql = "SELECT * FROM persons WHERE first_name=? AND last_name=?";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setString(1, firstName);
		stmt.setString(2, lastName);
		
		ResultSet rs = stmt.executeQuery();
		rs.first();
		int id = rs.getInt("id_person");
		
		return id;
	}
	/*
	 * Metode nodrošina SQL vaicājuma izveidi, kas attēlo personas pēc to epasta un paroles
	 * Metode atgriež person klases objektu, kas metodes izpildes procesā tiek aizpildīts ar vērtībām, kuras iegūst no vaicājuma.
	 * eMail - DB persons tabulas email lauka aizpildīšanas parametrs.
	 */
	public Person getPersonByLogin(String eMail, String password) throws SQLException {
		String sql = "SELECT * FROM persons WHERE email=? AND password=?";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setString(1, eMail);
		stmt.setString(2, password);
		
		ResultSet rs = stmt.executeQuery();
		rs.first();
		
		int id = rs.getInt("id_person");
		String firstName = rs.getString("first_name");
		String lastName = rs.getString("last_name");
		int courseID = rs.getInt("course_id");
		String role = rs.getString("role");
		
		Person person = new Person(firstName, lastName, eMail, password);
		person.setID(id);
		person.setRole(role);
		if(courseID != 0) {
			CourseService courseService = new CourseService(conn);
			Course course = courseService.getCourseByID(courseID);
			
			person.setCourse(course);
		}
		return person;
	}
	
	public List<Person> getPersonsByRole(String roleName) throws SQLException{
		List<Person> persons = new ArrayList<Person>();
		
		String sql = "SELECT * FROM persons WHERE role=?";
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setString(1, roleName);
		
		ResultSet rs = stmt.executeQuery();
		while(rs.next()) {
			int id = rs.getInt("id_person");
			String firstName = rs.getString("first_name");
			String lastName = rs.getString("last_name");
			String eMail = rs.getString("email");
			String password = rs.getString("password");
			int courseID = rs.getInt("course_id");
			
			Person person = new Person(firstName, lastName, eMail, password);
			person.setID(id);
			person.setRole(roleName);
			if(courseID != 0) {
				CourseService courseService = new CourseService(conn);
				Course course = courseService.getCourseByID(courseID);
				
				person.setCourse(course);
			}
			
			persons.add(person);
		}
		return persons;
	}
	
	// 1. uzdevums
	public List<Person> getPersonsByCourse(String courseName) throws SQLException{
		List<Person> persons = new ArrayList<Person>();
		CourseService coursServ = new CourseService(conn);
		String sql = "SELECT * FROM persons WHERE course_id=?";
		
		int courseID = coursServ.getCourseID(courseName);
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setInt(1, courseID);
		ResultSet rs = stmt.executeQuery();
		
		while(rs.next()) {
			int id = rs.getInt("id_person");
			String firstName = rs.getString("first_name");
			String lastName = rs.getString("last_name");
			String eMail = rs.getString("email");
			String password = rs.getString("password");
			String role = rs.getString("role");
			
			Person person = new Person(firstName, lastName, eMail, password);
			person.setID(id);
			person.setRole(role);
			if(courseID != 0) {
				CourseService courseService = new CourseService(conn);
				Course course = courseService.getCourseByID(courseID);
				
				person.setCourse(course);
			}
			
			persons.add(person);
		}
		return persons;

	}
	
	public List<Person> getAllPersons() throws SQLException{
		List<Person> persons = new ArrayList<Person>();
		
		String sql = "SELECT * FROM persons";
		
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while(rs.next()) {
			int id = rs.getInt("id_person");
			String firstName = rs.getString("first_name");
			String lastName = rs.getString("last_name");
			String eMail = rs.getString("email");
			String password = rs.getString("password");
			int courseID = rs.getInt("course_id");
			String role = rs.getString("role");
			
			Person person = new Person(firstName, lastName, eMail, password);
			person.setID(id);
			person.setRole(role);
			if(courseID != 0) {
				CourseService courseService = new CourseService(conn);
				Course course = courseService.getCourseByID(courseID);
				
				person.setCourse(course);
			}
			
			persons.add(person);
		}
		return persons;
	}
}
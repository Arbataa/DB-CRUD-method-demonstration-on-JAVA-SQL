package lv.jak.eksamens.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import lv.jak.eksamens.data.Subject;

public class SubjectService {
	private Connection conn;
	public SubjectService(Connection conn) {
		this.conn = conn;
	}
	
	public boolean insertSubject(String name) throws SQLException {
		String sql = "INSERT INTO subjects(subject_name) VALUES (?)";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setString(1, name);
		
		boolean result = stmt.execute();
		return result;
	}
	
	public int uptadeSubject(String oldName, String newName) throws SQLException {
		String sql = "UPDATE subjects set subject_name=? WHERE subject_name=?";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setString(1, oldName);
		stmt.setString(2, newName);
		
		int result = stmt.executeUpdate();
		return result;
	}
	
	// 1. uzdevums
	public int deleteSubject(String name) throws SQLException {
		String sql = "DELETE FROM subjects WHERE subject_name=?";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setString(1, name);
		
		int result = stmt.executeUpdate();
		return result;
	}
	
	public int getSubjectID(String name) throws SQLException {
        String sql = "SELECT id_subject FROM subjects WHERE subject_name=?";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setString(1, name);
		
		ResultSet rs = stmt.executeQuery();
		rs.first();
		
		int id = rs.getInt("id_subject");
		return id;
	}
	
	public Subject getSubjectByID(int id) throws SQLException {
		String sql = "SELECT * FROM subjects WHERE id_subject=?";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setInt(1,id);
		
		ResultSet rs = stmt.executeQuery();
		rs.first();
		
		String name = rs.getString("subject_name");		
		Subject subject = new Subject(name);
		subject.setID(id);
				
		return subject;
	}
	
	public List<Subject> getAllSubjects() throws SQLException {
		List<Subject> subjects = new ArrayList<Subject>();
		
		String sql = "SELECT * FROM subjects";
		
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while(rs.next()) {
			int id = rs.getInt("id_subject");
			String name = rs.getString("subject_name");
			
			Subject subject = new Subject(name);
			subject.setID(id);
			
			subjects.add(subject);
		}
		
		return subjects;
	}
}
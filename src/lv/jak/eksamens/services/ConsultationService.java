package lv.jak.eksamens.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lv.jak.eksamens.data.Consultation;
import lv.jak.eksamens.data.Person;
import lv.jak.eksamens.data.Subject;

public class ConsultationService {
	private Connection conn;
	
	public ConsultationService(Connection conn) {
		this.conn = conn;
	}
	
	public boolean insertConsultation(String studentFirst, String studentLast, String subjectName, 
			String teacherFirst, String teacherLast, String topic, Date dateOn, 
			boolean rewrite, String oldMark, Date rewriteDate) throws SQLException {
		
		PersonService personService = new PersonService(conn);
		int studentID = personService.getPersonID(studentFirst, studentLast);
		int teacherID = personService.getPersonID(teacherFirst, teacherLast);
		
		SubjectService subjectService = new SubjectService(conn);
		int subjectID = subjectService.getSubjectID(subjectName);
		
		String fields = "student_id, subject_id, teacher_id, topic, date_on";
		String values = "?, ?, ?, ?, ?";
		if(rewrite) {
			fields += ", rewrite";
			values += ", ?";
		}
		if(oldMark != null) {
			fields += ", old_mark";
			values += ", ?";
		}
		if(rewriteDate != null) {
			fields += ", rewrite_date";
			values += ", ?";
		}
		String sql = "INSERT INTO consultations(" + fields+ ") VALUES (" + values + ")";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setInt(1, studentID);
		stmt.setInt(2, subjectID);
		stmt.setInt(3, teacherID);
		stmt.setString(4, topic);
		stmt.setDate(5, (java.sql.Date) dateOn);
		
		int nr = 6;
		if(rewrite) {
			int rewriteValue = rewrite ? 1 : 0;
			stmt.setInt(nr, rewriteValue);
			nr++;
		}
		if(oldMark != null) {stmt.setString(nr, oldMark);nr++;}
		if(rewriteDate != null) {stmt.setDate(nr,(java.sql.Date) rewriteDate);nr++;}
		
		
		boolean result = stmt.execute();
		return result;
	}
	
	public int deleteConsultation(String studentFirst, String studentLast, String subjectName, 
			String teacherFirst, String teacherLast, Date dateOn) throws SQLException {
		PersonService personService = new PersonService(conn);
		int studentID = personService.getPersonID(studentFirst, studentLast);
		int teacherID = personService.getPersonID(teacherFirst, teacherLast);
		
		SubjectService subjectService = new SubjectService(conn);
		int subjectID = subjectService.getSubjectID(subjectName);
		
		String sql = "DELETE FROM consultations WHERE student_id=? AND subject_id=? AND teacher_id=? AND date_on=?";
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setInt(1, studentID);
		stmt.setInt(2, subjectID);
		stmt.setInt(3, teacherID);
		stmt.setDate(4, (java.sql.Date) dateOn);
		
		int result = stmt.executeUpdate();
		return result;
	}
	
	public int getConsultationID(String studentFirst, String studentLast, String subjectName, 
			String teacherFirst, String teacherLast, Date dateOn) throws SQLException {
		PersonService personService = new PersonService(conn);
		int studentID = personService.getPersonID(studentFirst, studentLast);
		int teacherID = personService.getPersonID(teacherFirst, teacherLast);
		
		SubjectService subjectService = new SubjectService(conn);
		int subjectID = subjectService.getSubjectID(subjectName);
		
        String sql = "SELECT id_consultation FROM consultations WHERE student_id=? AND subject_id=? AND teacher_id=? AND date_on=?";
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setInt(1, studentID);
		stmt.setInt(2, subjectID);
		stmt.setInt(3, teacherID);
		stmt.setDate(4, (java.sql.Date) dateOn);
		
		ResultSet rs = stmt.executeQuery();
		rs.first();
		
		int id = rs.getInt("id_consultation");
		return id;
	}
	
	public Consultation getConsultationByID(int id) throws SQLException {
		String sql = "SELECT * FROM consultations WHERE id_consultation=?";
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setInt(1, id);
		
		ResultSet rs = stmt.executeQuery();
		rs.first();
		
		int studentID = rs.getInt("student_id");
		int subjectID = rs.getInt("subject_id");
		int teacherID = rs.getInt("teacher_id");
		String topic = rs.getString("topic");
		Date dateOn = rs.getDate("date_on");
		boolean rewrite = rs.getBoolean("rewrite");
		String oldMark = rs.getString("old_mark");
		Date rewriteDate = rs.getDate("rewrite_date");
		
		PersonService personService = new PersonService(conn);
		Person student = personService.getPersonByID(studentID);
		Person teacher = personService.getPersonByID(teacherID);
		
		SubjectService subjectService = new SubjectService(conn);
		Subject subject = subjectService.getSubjectByID(subjectID);
		
		Consultation consultation = new Consultation(student, subject, teacher, topic, dateOn);
		consultation.setId(id);
		consultation.setRewrite(rewrite);
		consultation.setOldMark(oldMark);
		consultation.setRewriteDate(rewriteDate);
		
		return consultation;
	}
	
	public List<Consultation> getConsultationsBySubject(String subjectName) throws SQLException {
		List<Consultation> consultations = new ArrayList<Consultation>();
		
		SubjectService subjectService = new SubjectService(conn);
		int subjectID = subjectService.getSubjectID(subjectName);
		
		String sql = "SELECT * FROM consultations WHERE subject_id=?";
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setInt(1, subjectID);
		
		ResultSet rs = stmt.executeQuery();
		
		while(rs.next()) {
			int id = rs.getInt("id_consultation");
			int studentID = rs.getInt("student_id");
			int teacherID = rs.getInt("teacher_id");
			String topic = rs.getString("topic");
			Date dateOn = rs.getDate("date_on");
			boolean rewrite = rs.getBoolean("rewrite");
			String oldMark = rs.getString("old_mark");
			Date rewriteDate = rs.getDate("rewrite_date");
			
			PersonService personService = new PersonService(conn);
			Person student = personService.getPersonByID(studentID);
			Person teacher = personService.getPersonByID(teacherID);
			
			Subject subject = subjectService.getSubjectByID(subjectID);
			
			Consultation consultation = new Consultation(student, subject, teacher, topic, dateOn);
			consultation.setId(id);
			consultation.setRewrite(rewrite);
			consultation.setOldMark(oldMark);
			consultation.setRewriteDate(rewriteDate);
			
			consultations.add(consultation);
		}
		
		return consultations;
	}
	
	public List<Consultation> getConsultationsByStudent(String firstName, String lastName, String role) throws SQLException {
		List<Consultation> consultations = new ArrayList<Consultation>();
		
		PersonService personService = new PersonService(conn);
		int personID = personService.getPersonID(firstName, lastName);
		
		String criteria = "";
		if(role.equals("Audzēknis"))
			criteria = "student_id";
		else if(role.equals("Skolotājs"))
			criteria = "teacher_id";
		
		String sql = "SELECT * FROM consultations WHERE " + criteria + "=?";
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setInt(1, personID);
		
		ResultSet rs = stmt.executeQuery();
		
		while(rs.next()) {
			int id = rs.getInt("id_consultation");
			int studentID = rs.getInt("student_id");
			int subjectID = rs.getInt("subject_id");
			int teacherID = rs.getInt("teacher_id");
			String topic = rs.getString("topic");
			Date dateOn = rs.getDate("date_on");
			boolean rewrite = rs.getBoolean("rewrite");
			String oldMark = rs.getString("old_mark");
			Date rewriteDate = rs.getDate("rewrite_date");
			
			Person student = personService.getPersonByID(studentID);
			Person teacher = personService.getPersonByID(teacherID);
			
			SubjectService subjectService = new SubjectService(conn);
			Subject subject = subjectService.getSubjectByID(subjectID);
			
			Consultation consultation = new Consultation(student, subject, teacher, topic, dateOn);
			consultation.setId(id);
			consultation.setRewrite(rewrite);
			consultation.setOldMark(oldMark);
			consultation.setRewriteDate(rewriteDate);
			
			consultations.add(consultation);
		}
		
		return consultations;
	}
	
	public List<Consultation> getAllConsultations() throws SQLException {
		List<Consultation> consultations = new ArrayList<Consultation>();
		
		String sql = "SELECT * FROM consultations";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while(rs.next()) {
			int id = rs.getInt("id_consultation");
			int studentID = rs.getInt("student_id");
			int subjectID = rs.getInt("subject_id");
			int teacherID = rs.getInt("teacher_id");
			String topic = rs.getString("topic");
			Date dateOn = rs.getDate("date_on");
			boolean rewrite = rs.getBoolean("rewrite");
			String oldMark = rs.getString("old_mark");
			Date rewriteDate = rs.getDate("rewrite_date");
			
			PersonService personService = new PersonService(conn);
			Person student = personService.getPersonByID(studentID);
			Person teacher = personService.getPersonByID(teacherID);
			
			SubjectService subjectService = new SubjectService(conn);
			Subject subject = subjectService.getSubjectByID(subjectID);
			
			Consultation consultation = new Consultation(student, subject, teacher, topic, dateOn);
			consultation.setId(id);
			consultation.setRewrite(rewrite);
			consultation.setOldMark(oldMark);
			consultation.setRewriteDate(rewriteDate);
			
			consultations.add(consultation);
		}
		
		return consultations;
	}
}
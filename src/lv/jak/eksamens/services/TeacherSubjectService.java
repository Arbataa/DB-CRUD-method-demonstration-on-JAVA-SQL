package lv.jak.eksamens.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import lv.jak.eksamens.data.Person;
import lv.jak.eksamens.data.Subject;
import lv.jak.eksamens.data.TeacherSubject;

public class TeacherSubjectService {
	private Connection conn;
	public TeacherSubjectService(Connection conn) {
		this.conn = conn;
	}
	
	public boolean insertTeacherSubject(String firstName, String lastName, String subject) throws SQLException {
		PersonService personService = new PersonService(conn);
		int teacherID = personService.getPersonID(firstName, lastName);
		
		SubjectService subjectService = new SubjectService(conn);
		int subjectID = subjectService.getSubjectID(subject);
		
		String sql = "INSERT INTO teacher_subjects(teacher_id, subject_id) VALUES (?, ?)";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setInt(1, teacherID);
		stmt.setInt(2, subjectID);
		
		boolean result = stmt.execute();
		return result;
	}
	
	public int updateTeacherSubject(String oldFirstName, String oldLastName, String subject, String newFirstName, String newLastName) throws SQLException {
		PersonService personService = new PersonService(conn);
		int oldTeacherID = personService.getPersonID(oldFirstName, oldLastName);
		int newTeacherID = personService.getPersonID(newFirstName, newLastName);
		
		SubjectService subjectService = new SubjectService(conn);
		int subjectID = subjectService.getSubjectID(subject);
		
		String sql = "UPDATE teacher_subjects SET teacher_id=? WHERE teacher_id=? AND subject_id=?";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setInt(1, newTeacherID);
		stmt.setInt(2, oldTeacherID);
		stmt.setInt(3, subjectID);
		
		int result = stmt.executeUpdate();
		return result;
	}
	
	public int updateTeacherSubject(String firstName, String lastName, String oldSubject, String newSubject) throws SQLException {
		PersonService personService = new PersonService(conn);
		int teacherID = personService.getPersonID(firstName, lastName);
		
		SubjectService subjectService = new SubjectService(conn);
		int oldSubjectID = subjectService.getSubjectID(oldSubject);
		int newSubjectID = subjectService.getSubjectID(newSubject);
		
		String sql = "UPDATE teacher_subjects SET subject_id=? WHERE teacher_id=? AND subject_id=?";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setInt(1, newSubjectID);
		stmt.setInt(2, teacherID);
		stmt.setInt(3, oldSubjectID);
		
		int result = stmt.executeUpdate();
		return result;
	}
	
	public int deleteTeacherSubject(String firstName, String lastName, String subjectName) throws SQLException {
		PersonService personService = new PersonService(conn);
		int teacherID = personService.getPersonID(firstName, lastName);
		
		SubjectService subjectService = new SubjectService(conn);
		int subjectID = subjectService.getSubjectID(subjectName);
		
		String sql = "DELETE FROM teacher_subjects WHERE teacher_id=? AND subject_id=?";
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setInt(1, teacherID);
		stmt.setInt(2, subjectID);
		
		int result = stmt.executeUpdate();
		return result;
	}
	
	public TeacherSubject getTeacherSubjectByID(int id) throws SQLException {
		String sql = "SELECT teacher_id, subject_id FROM teacher_subjects WHERE id_ts=?";
		
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setInt(1, id);
		
		ResultSet rs = stmt.executeQuery();
		rs.first();
		int teacherID = rs.getInt("teacher_id");
		int subjectID = rs.getInt("prieksmets_id");
		
		PersonService personService = new PersonService(conn);
		Person teacher = personService.getPersonByID(teacherID);
		
		SubjectService subjectService = new SubjectService(conn);
		Subject subject = subjectService.getSubjectByID(subjectID);
		
		TeacherSubject ts = new TeacherSubject(teacher, subject);
		
		return ts;
	}
	
	public int getTeacherSubjectID(String firstName, String lastName, String subjectName) throws SQLException {
		PersonService personService = new PersonService(conn);
		int teacherID = personService.getPersonID(firstName, lastName);
		
		SubjectService subjectService = new SubjectService(conn);
		int subjectID = subjectService.getSubjectID(subjectName);
		
		String sql = "SELECT id_ts FROM teacher_subjects WHERE teacher_id=? AND subject_id=?";
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setInt(1, teacherID);
		stmt.setInt(2, subjectID);
		
		ResultSet rs = stmt.executeQuery();
		rs.first();
		int id = rs.getInt("id_ts");
		
		return id;
	}
	
	public List<TeacherSubject> getSkolotajiPrieksmetiByPrieksmets(String subjectName) throws SQLException{
		List<TeacherSubject> teacherSubjects = new ArrayList<TeacherSubject>();
		
		PersonService personService = new PersonService(conn);
		SubjectService subjectService = new SubjectService(conn);
		int subjectID = subjectService.getSubjectID(subjectName);
		
		String sql = "SELECT teacher_id FROM teacher_subjects WHERE subject_id=?";
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setInt(1, subjectID);
		
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			int teacherID = rs.getInt("teacher_id");
			Person teacher = personService.getPersonByID(teacherID);
			Subject subject = subjectService.getSubjectByID(subjectID);
			
			TeacherSubject teacherSubject = new TeacherSubject(teacher, subject);
			teacherSubjects.add(teacherSubject);
		}
		
		return teacherSubjects;
	}
	
	public List<TeacherSubject> getSkolotajiPrieksmetiBySkolotajs(String firstName, String lastName) throws SQLException{
		List<TeacherSubject> teacherSubjects = new ArrayList<TeacherSubject>();
		
		SubjectService subjectService = new SubjectService(conn);
		PersonService personService = new PersonService(conn);
		int teacherID = personService.getPersonID(firstName, lastName);
		
		String sql = "SELECT subject_id FROM teacher_subjects WHERE teacher_id=?";
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		stmt.setInt(1, teacherID);
		
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			int subjectID = rs.getInt("subject_id");
			Subject subject = subjectService.getSubjectByID(subjectID);
			Person teacher = personService.getPersonByID(teacherID);
			
			TeacherSubject teacherSubject = new TeacherSubject(teacher, subject);
			teacherSubjects.add(teacherSubject);
		}
		
		return teacherSubjects;
	}
	
	public List<TeacherSubject> getAllTeacherSubjects() throws SQLException{
		List<TeacherSubject> teacherSubjects = new ArrayList<TeacherSubject>();
		
		PersonService personService = new PersonService(conn);
		SubjectService subjectService = new SubjectService(conn);
		
		String sql = "SELECT * FROM teacher_subjects";
		Statement stmt = conn.createStatement();
		
		ResultSet rs = stmt.executeQuery(sql);
		while (rs.next()) {
			int id = rs.getInt("id_ts");
			int teacherID = rs.getInt("teacher_id");
			int subjectID = rs.getInt("subject_id");

			Person teacher = personService.getPersonByID(teacherID);
			Subject subject = subjectService.getSubjectByID(subjectID);
			
			TeacherSubject teacherSubject = new TeacherSubject(teacher, subject);
			teacherSubject.setID(id);
			
			teacherSubjects.add(teacherSubject);
		}
		
		return teacherSubjects;
	}
}
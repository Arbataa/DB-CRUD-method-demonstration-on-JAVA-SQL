package lv.jak.eksamens;

import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.SQLException;

import lv.jak.eksamens.base.Database;
import lv.jak.eksamens.forms.LoginForm;

public class MainClass {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Connection conn = Database.connect("konsultacijas");
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginForm frame = new LoginForm(conn);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
package lv.jak.eksamens.data;

public class Course{
	private int id;
	private String name;
	
	public Course(String name){
		this.name = name;
	}
	
	public void setID(int id){
		this.id = id;
	}
	
	public int getID(){
		return this.id;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
}
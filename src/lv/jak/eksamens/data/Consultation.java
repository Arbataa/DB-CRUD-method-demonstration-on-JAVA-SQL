package lv.jak.eksamens.data;

import java.util.Date;

public class Consultation {
	private int id;
	private Person student;
	private Subject subject;
	private Person teacher;
	private String topic;
	private Date dateOn;
	private boolean rewrite;
	private Date rewriteDate;
	private String oldMark;
	
	public Consultation(Person student, Subject subject, Person teacher, String topic, Date dateOn) {
		this.student = student;
		this.subject = subject;
		this.teacher = teacher;
		this.topic = topic;
		this.dateOn = dateOn;
		this.rewrite = false;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Person getStudent() {
		return student;
	}

	public void setStudent(Person student) {
		this.student = student;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Person getTeacher() {
		return teacher;
	}

	public void setTeacher(Person teacher) {
		this.teacher = teacher;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public Date getDateOn() {
		return dateOn;
	}

	public void setDateOn(Date dateOn) {
		this.dateOn = dateOn;
	}

	public boolean isRewrite() {
		return rewrite;
	}

	public void setRewrite(boolean rewrite) {
		this.rewrite = rewrite;
	}

	public Date getRewriteDate() {
		return rewriteDate;
	}

	public void setRewriteDate(Date rewriteDate) {
		this.rewriteDate = rewriteDate;
	}

	public String getOldMark() {
		return oldMark;
	}

	public void setOldMark(String oldMark) {
		this.oldMark = oldMark;
	}
}
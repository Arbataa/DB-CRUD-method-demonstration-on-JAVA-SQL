package lv.jak.eksamens.data;

import lv.jak.eksamens.data.Course;

public class Person{
	private int id;
	private String firstName;
	private String lastName;
	private String eMail;
	private String password;
	private Course course;
	private String role;
	
	public Person(String firstName, String lastName, String eMail, String password){
		this.firstName = firstName;
		this.lastName = lastName;
		this.eMail = eMail;
		this.password = password;
	}
	
	public void setID(int id){
		this.id = id;
	}
	
	public int getID(){
		return this.id;
	}
	
	public void setFirstName(String firstName){
		this.firstName = firstName;
	}
	
	public String getFirstName(){
		return this.firstName;
	}
	
	public void setLastName(String lastName){
		this.lastName = lastName;
	}
	
	public String getLastName(){
		return this.lastName;
	}
	
	public void setEmail(String eMail){
		this.eMail = eMail;
	}
	
	public String getEmail(){
		return this.eMail;
	}
	
	public void setPassword(String password){
		this.password = password;
	}
	
	public String getPassword(){
		return this.password;
	}
	
	public void setCourse(Course course){
		this.course = course;
	}
	
	public Course getCourse(){
		return this.course;
	}
	
	public void setRole(String role){
		this.role = role;
	}
	
	public String getRole(){
		return this.role;
	}
}
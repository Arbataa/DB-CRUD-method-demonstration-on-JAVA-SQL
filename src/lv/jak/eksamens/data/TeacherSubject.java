package lv.jak.eksamens.data;

public class TeacherSubject {
	private int id;
	private Person teacher;
	private Subject subject;
	
	public TeacherSubject(Person teacher, Subject subject) {
		this.teacher = teacher;
		this.subject = subject;
	}

	public int getID() {
		return id;
	}

	public void setID(int id) {
		this.id = id;
	}

	public Person getTeacher() {
		return teacher;
	}

	public void setTeacher(Person teacher) {
		this.teacher = teacher;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}
}
package lv.jak.eksamens.test;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import lv.jak.eksamens.base.Database;
import lv.jak.eksamens.data.Course;
import lv.jak.eksamens.services.CourseService;

	class CourseServiceUT {
	private CourseService courseServ;
	String goodName,badName;
	int counter = 0;
	int coursesLength;
	int coursesLengthTest;
	List<Course> coursesTest = new ArrayList<Course>(); 
	
	@Test
	void testCourseService() throws ClassNotFoundException, SQLException {
		Connection conn = Database.connect("konsultacijas");
		CourseService courseServ = new CourseService(conn);
		
		assertNotNull(courseServ, "Nav inicializēts klaseService objekts");
		assertNotNull(courseServ.conn,"conn objekts nav inicializēts");
	}
	
	@BeforeEach
	void init() throws ClassNotFoundException, SQLException{
		Connection conn = Database.connect("konsultacijas");
		courseServ = new CourseService(conn);
		
		String sql = "SELECT * FROM courses";
		PreparedStatement stmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		ResultSet rs = stmt.executeQuery();
		
		while(rs.next()) {
			int id = rs.getInt("id_course");
			String name = rs.getString("course_name");
			
			Course course = new Course(name);
			course.setID(id);
			
			coursesTest.add(course);
		}
		goodName = "2.p";
		badName = "test";
	
		coursesLengthTest = coursesTest.size();
	}

	@Test
	void testGetCourseID() throws SQLException {
		int labsID = courseServ.getCourseID(goodName);
		assertEquals(1, labsID,"Metode atgriež nepareizu ID");
		assertThrows(SQLException.class, ()->courseServ.getCourseID(badName), "Tika atrasta neeksistējošs kurss");
	}

	@Test
	void testGetAllCourses() throws SQLException {
		List<Course> courses = new ArrayList<Course>();
		courses.addAll(courseServ.getAllCourses());
		coursesLengthTest = courses.size();
		
		assertNotNull(courses, "Metodes izpildes procesā konteinerobjekti netiek aizpildīti.");
		
		for (Course course : coursesTest) {
			assertEquals(course.getName(), courses.get(counter).getName(),"Metode aizpilda sarakstu ar nepareizu vērtību (name)  pie" + counter + " ideksa");
			
		}
		
		for (Course course : coursesTest) {
			assertEquals(course.getID(), courses.get(counter).getID(),"Metode aizpilda sarakstu ar nepareizu vērtību (ID)  pie" + counter + " ideksa");
			
		}

		assertEquals(coursesLengthTest, coursesLengthTest, "Tiek izveidots courses saraksts ar nepareizu garumu");
	}

}

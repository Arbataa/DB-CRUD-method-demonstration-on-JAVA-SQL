package lv.jak.eksamens.base;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Database {
	public static Connection connect(String nameDB) throws SQLException, ClassNotFoundException {
		Properties prop = new Properties();
		prop.put("charSet", "utf-8");
		prop.put("user", "root");
		prop.put("password", "");

		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + nameDB, prop);

		return conn;
	}
}